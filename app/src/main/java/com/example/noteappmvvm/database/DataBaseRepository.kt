package com.example.noteappmvvm.database

import androidx.lifecycle.LiveData
import com.example.noteappmvvm.viewmodel.modelnote.NoteViewModel

interface DataBaseRepository {

    // Считывает данные с базы
    val readAll: LiveData<List<NoteViewModel>>

    suspend fun create(note: NoteViewModel, onSuccess: () -> Unit)
    suspend fun update(note: NoteViewModel, onSuccess: () -> Unit)
    suspend fun delete(note: NoteViewModel, onSuccess: () -> Unit)
    fun signOut() {}
    fun connectToDatabase(onSuccess: () -> Unit, onFail: (String) -> Unit) {}
}