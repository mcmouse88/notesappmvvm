package com.example.noteappmvvm.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.noteappmvvm.room.dao.NoteRoomDao
import com.example.noteappmvvm.utils.Constants.Keys.NOTE_DATABASE
import com.example.noteappmvvm.viewmodel.modelnote.NoteViewModel

@Database(entities = [NoteViewModel::class], version = 1)
abstract class AppRoomDatabase : RoomDatabase() {
    abstract fun getRoomDao(): NoteRoomDao

    companion object {
        @Volatile
        private var instance: AppRoomDatabase? = null

        fun getInstance(context: Context): AppRoomDatabase {
            return if (instance == null) {
                instance = Room.databaseBuilder(
                    context,
                    AppRoomDatabase::class.java,
                    NOTE_DATABASE
                ).build()
                instance as AppRoomDatabase
            } else instance as AppRoomDatabase
        }
    }
}