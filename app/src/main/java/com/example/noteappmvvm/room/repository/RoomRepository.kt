package com.example.noteappmvvm.room.repository

import androidx.lifecycle.LiveData
import com.example.noteappmvvm.database.DataBaseRepository
import com.example.noteappmvvm.room.dao.NoteRoomDao
import com.example.noteappmvvm.viewmodel.modelnote.NoteViewModel

class RoomRepository(private val noteRoomDao: NoteRoomDao) : DataBaseRepository {
    override val readAll: LiveData<List<NoteViewModel>>
        get() = noteRoomDao.getAllNotes()

    override suspend fun create(note: NoteViewModel, onSuccess: () -> Unit) {
        noteRoomDao.addNote(note = note)
        onSuccess()
    }

    override suspend fun update(note: NoteViewModel, onSuccess: () -> Unit) {
        noteRoomDao.updateNote(note = note)
        onSuccess()
    }

    override suspend fun delete(note: NoteViewModel, onSuccess: () -> Unit) {
        noteRoomDao.deleteNote(note = note)
        onSuccess()
    }
}