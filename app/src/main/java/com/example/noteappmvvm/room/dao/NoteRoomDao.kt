package com.example.noteappmvvm.room.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.noteappmvvm.viewmodel.modelnote.NoteViewModel

@Dao
interface NoteRoomDao {

    @Query("select * from notes_table")
    fun getAllNotes(): LiveData<List<NoteViewModel>>

    @Insert
    suspend fun addNote(note: NoteViewModel)

    @Update
    suspend fun updateNote(note: NoteViewModel)

    @Delete
    suspend fun deleteNote(note: NoteViewModel)
}