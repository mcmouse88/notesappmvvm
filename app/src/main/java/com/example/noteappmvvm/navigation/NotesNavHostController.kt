package com.example.noteappmvvm.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.noteappmvvm.screens.*
import com.example.noteappmvvm.utils.Constants
import com.example.noteappmvvm.utils.Screens.ADD_SCREEN
import com.example.noteappmvvm.utils.Screens.MAIN_SCREEN
import com.example.noteappmvvm.utils.Screens.NOTE_SCREEN
import com.example.noteappmvvm.utils.Screens.START_SCREEN
import com.example.noteappmvvm.viewmodel.MainViewModel

sealed class NavRoute(val route: String) {
    object Start : NavRoute(START_SCREEN)
    object Main : NavRoute(MAIN_SCREEN)
    object Add : NavRoute(ADD_SCREEN)
    object Note : NavRoute(NOTE_SCREEN)
}

@Composable
fun NotesNavHostController(mainViewModel: MainViewModel) {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = NavRoute.Start.route) {
        composable(NavRoute.Start.route) { StartScreen(navHostController = navController, viewModel = mainViewModel) }
        composable(NavRoute.Main.route) { MainScreen(navHostController = navController, viewModel = mainViewModel) }
        composable(NavRoute.Add.route) { AddScreen(navHostController = navController, viewModel = mainViewModel) }
        composable(NavRoute.Note.route + "/{${Constants.Keys.ID}}") {
            NoteScreen(navHostController = navController, viewModel = mainViewModel, noteId = it.arguments?.getString(Constants.Keys.ID))
        }
    }

}