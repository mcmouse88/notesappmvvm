package com.example.noteappmvvm.firedatabase

import androidx.lifecycle.LiveData
import com.example.noteappmvvm.viewmodel.modelnote.NoteViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import java.time.temporal.ValueRange

class AllNotesLiveData : LiveData<List<NoteViewModel>>() {
    private val mAuth = FirebaseAuth.getInstance()
    private val firebase = Firebase.database.reference
        .child(mAuth.currentUser?.uid.toString())

    private val listener = object : ValueEventListener {
        override fun onDataChange(snapshot: DataSnapshot) {
            val notes = mutableListOf<NoteViewModel>()
            snapshot.children.map {
                notes.add(it.getValue(NoteViewModel::class.java) ?: NoteViewModel())
            }
            value = notes
        }

        override fun onCancelled(error: DatabaseError) {}
    }

    override fun onActive() {
        firebase.addValueEventListener(listener)
        super.onActive()
    }

    override fun onInactive() {
        firebase.removeEventListener(listener)
        super.onInactive()
    }
}