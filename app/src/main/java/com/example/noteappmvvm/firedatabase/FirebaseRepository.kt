package com.example.noteappmvvm.firedatabase

import android.util.Log
import androidx.lifecycle.LiveData
import com.example.noteappmvvm.database.DataBaseRepository
import com.example.noteappmvvm.utils.Constants.Keys.SUBTITLE
import com.example.noteappmvvm.utils.Constants.Keys.TITLE
import com.example.noteappmvvm.utils.FIREBASE_ID
import com.example.noteappmvvm.utils.LOGIN
import com.example.noteappmvvm.utils.PASSWORD
import com.example.noteappmvvm.viewmodel.modelnote.NoteViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

class FirebaseRepository : DataBaseRepository {
    private val mAuth = FirebaseAuth.getInstance()
    private val firebase = Firebase.database.reference
        .child(mAuth.currentUser?.uid.toString())

    override val readAll: LiveData<List<NoteViewModel>>
        get() = AllNotesLiveData()

    override suspend fun create(note: NoteViewModel, onSuccess: () -> Unit) {
        val noteId = firebase.push().key.toString()
        val mapNotes = hashMapOf<String, Any>()

        mapNotes[FIREBASE_ID] = noteId
        mapNotes[TITLE] = note.title
        mapNotes[SUBTITLE] = note.subtitle

        firebase.child(noteId)
            .updateChildren(mapNotes)
            .addOnSuccessListener { onSuccess() }
            .addOnFailureListener { Log.d("CheckData", "Failed to add new note") }
    }

    override suspend fun update(note: NoteViewModel, onSuccess: () -> Unit) {
        val noteId = note.firebaseId
        val mapNotes = hashMapOf<String, Any>()

        mapNotes[FIREBASE_ID] = noteId
        mapNotes[TITLE] = note.title
        mapNotes[SUBTITLE] = note.subtitle

        firebase.child(noteId)
            .updateChildren(mapNotes)
            .addOnSuccessListener { onSuccess() }
            .addOnFailureListener { Log.d("CheckData", "Failed to update new note") }
    }

    override suspend fun delete(note: NoteViewModel, onSuccess: () -> Unit) {
        firebase.child(note.firebaseId)
            .removeValue()
            .addOnSuccessListener { onSuccess() }
            .addOnFailureListener { Log.d("CheckData", "Failed to delete new note") }
    }

    override fun signOut() {
        mAuth.signOut()
    }

    override fun connectToDatabase(onSuccess: () -> Unit, onFail: (String) -> Unit) {
        mAuth.signInWithEmailAndPassword(LOGIN, PASSWORD)
            .addOnSuccessListener { onSuccess() }
            .addOnFailureListener {
                mAuth.createUserWithEmailAndPassword(LOGIN, PASSWORD)
                    .addOnSuccessListener { onSuccess() }
                    .addOnFailureListener { it.message.toString() }
            }
    }
}