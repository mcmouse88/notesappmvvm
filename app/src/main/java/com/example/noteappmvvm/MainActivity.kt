package com.example.noteappmvvm

import android.app.Application
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.width
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.filled.Person
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.compose.rememberNavController
import com.example.noteappmvvm.navigation.NotesNavHostController
import com.example.noteappmvvm.ui.theme.NoteAppMVVMTheme
import com.example.noteappmvvm.viewmodel.MainViewModel
import com.example.noteappmvvm.viewmodel.ViewModelFactory

class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            NoteAppMVVMTheme {
                // A surface container using the 'background' color from the theme
                val navController = rememberNavController()
                val context = LocalContext.current
                val factory = ViewModelFactory(context.applicationContext as Application)
                val mainViewModel: MainViewModel =
                    viewModel(factory = factory)
                Scaffold(
                    topBar = {
                        TopAppBar(
                            title = {
                                Text(text = "Notes App")

                            },
                            navigationIcon = {
                                IconButton(onClick = {
                                    super.onBackPressed()
                                }) {
                                    Icon(
                                        imageVector = Icons.Filled.ArrowBack,
                                        contentDescription = "Back"
                                    )
                                }
                            },
                            backgroundColor = Color.Blue,
                            contentColor = Color.White,
                            elevation = 12.dp
                        )
                    },
                    content = {
                        Surface(
                            modifier = Modifier.fillMaxSize(),
                            color = MaterialTheme.colors.background
                        ) {
                            NotesNavHostController(mainViewModel)
                        }
                    }
                )
            }
        }
    }
}


