package com.example.noteappmvvm.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.example.noteappmvvm.firedatabase.FirebaseRepository
import com.example.noteappmvvm.room.AppRoomDatabase
import com.example.noteappmvvm.room.repository.RoomRepository
import com.example.noteappmvvm.utils.REPOSITORY
import com.example.noteappmvvm.utils.TYPE_FIREBASE
import com.example.noteappmvvm.utils.TYPE_ROOM
import com.example.noteappmvvm.viewmodel.modelnote.NoteViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel(application: Application) : AndroidViewModel(application) {

    private val context = application

    fun initDatabase(type: String, onSuccess: () -> Unit) {
        Log.d("CheckData", "MainView Model init database with type: $type")
        when (type) {
            TYPE_ROOM -> {
                val dao = AppRoomDatabase.getInstance(context = context).getRoomDao()
                REPOSITORY = RoomRepository(dao)
                onSuccess()
            }
            TYPE_FIREBASE -> {
                REPOSITORY = FirebaseRepository()
                REPOSITORY.connectToDatabase(
                    { onSuccess() },
                    { Log.d("CheckData", "Error: $it") }
                )
            }
        }
    }

    fun addNote(note: NoteViewModel, onSuccess: () -> Unit) {
        viewModelScope.launch(Dispatchers.IO) {
            REPOSITORY.create(note = note) {
                viewModelScope.launch(Dispatchers.Main) { onSuccess() }
            }
        }
    }

    fun deleteNote(note: NoteViewModel, onSuccess: () -> Unit) {
        viewModelScope.launch(Dispatchers.IO) {
            REPOSITORY.delete(note = note) {
                viewModelScope.launch(Dispatchers.Main) { onSuccess() }
            }
        }
    }

    fun readAllNotes() = REPOSITORY.readAll

    fun updateNote(note: NoteViewModel, onSuccess: () -> Unit) {
        viewModelScope.launch(Dispatchers.IO) {
            REPOSITORY.update(note = note) {
                viewModelScope.launch(Dispatchers.Main) { onSuccess() }
            }
        }
    }
}