package com.example.noteappmvvm.screens

import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.example.noteappmvvm.navigation.NavRoute
import com.example.noteappmvvm.utils.Constants.Keys.ADD_NEW_NOTE
import com.example.noteappmvvm.utils.Constants.Keys.ADD_NOTE
import com.example.noteappmvvm.utils.Constants.Keys.NOTE_SUBTITLE
import com.example.noteappmvvm.utils.Constants.Keys.NOTE_TITLE
import com.example.noteappmvvm.viewmodel.MainViewModel
import com.example.noteappmvvm.viewmodel.modelnote.NoteViewModel

@Composable
fun AddScreen(navHostController: NavHostController, viewModel: MainViewModel) {
    var title by remember { mutableStateOf("") }
    var subtitle by remember { mutableStateOf("") }
    var isButtonEnabled by remember { mutableStateOf(false) }
    Scaffold {
        Column(
            modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Text(
                text = ADD_NEW_NOTE,
                fontSize = 18.sp,
                fontWeight = FontWeight.Bold,
                modifier = Modifier.padding(vertical = 8.dp)
            )
            OutlinedTextField(
                value = title,
                onValueChange = {
                    title = it
                    isButtonEnabled = title.isNotEmpty() && subtitle.isNotEmpty()
                },
                label = { Text(text = NOTE_TITLE) },
                isError = title.isEmpty()
            )
            OutlinedTextField(
                value = subtitle,
                onValueChange = {
                    subtitle = it
                    isButtonEnabled = title.isNotEmpty() && subtitle.isNotEmpty()
                },
                label = { Text(text = NOTE_SUBTITLE) },
                isError = subtitle.isEmpty()
            )
            Button(
                modifier = Modifier.padding(top = 16.dp),
                enabled = isButtonEnabled,
                onClick = {
                    viewModel.addNote(note = NoteViewModel(title = title, subtitle = subtitle)) {
                        navHostController.navigate(NavRoute.Main.route)
                    }
                }
            ) {
                Text(text = ADD_NOTE)
            }
        }
    }
}